//https://learn.adafruit.com/adafruit-neopixel-uberguide/neomatrix-library

#include <Arduino.h>

#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include "RGB.h"
#include "functions.h"

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>

#include "Timers.h"
Timer batteryTimer;

//////////WIFI//////////////

#include "ArduinoJson.h"

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>


#ifndef STASSID
#define STASSID "Majutek_2.4G"
#define STAPSK  "Czekolada01"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

ESP8266WebServer server(80);

void handleRoot() {
  digitalWrite(D4, 1);
  server.send(200, "text/plain", "hello from esp8266!");
  digitalWrite(D4, 0);
}

void handleNotFound() {
  digitalWrite(D4, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(D4, 0);
}


struct led {
   
    int column;
    int row;
    int color;
} led_resource;


void get_leds() {

    StaticJsonDocument<200> doc;
    JsonObject jsonObj = doc.to<JsonObject>();
  
    jsonObj["column"] = led_resource.column;
    jsonObj["row"] = led_resource.row;
    jsonObj["color"] = led_resource.color;
    
    String output;
    serializeJsonPretty(jsonObj, output);
            
    server.send(200, "application/json", output);    
}

void json_to_resource(JsonObject jsonBody) {
    int column, row, color;

    column = jsonBody["column"];
    row = jsonBody["row"];
    color = jsonBody["color"];

    Serial.println(column);
    Serial.println(row);
    Serial.println(color);

    led_resource.column = column;
    led_resource.row = row;
    led_resource.color = color;
}

void post_put_leds() {

    StaticJsonDocument<500> doc;

    String post_body = server.arg("plain");
    Serial.println(post_body);

    DeserializationError err = deserializeJson(doc, post_body);        

    Serial.print("HTTP Method: ");
    Serial.println(server.method());
   
    if (err) {
      Serial.print(F("deserializeJson() failed with code "));
      Serial.println(err.c_str());
      server.send(400);
      }       
    else {   
        if (server.method() == HTTP_POST) {
          Serial.println("POST!!!!!!");
            // if ((jsonBody["id"] != 0) && (jsonBody["id"] != led_resource.id)) {
            //     json_to_resource(jsonBody);
            //     http_rest_server.sendHeader("Location", "/leds/" + String(led_resource.id));
            //     http_rest_server.send(201);
            //     pinMode(led_resource.gpio, OUTPUT);
            // }
            // else if (jsonBody["id"] == 0)
            //   http_rest_server.send(404);
            // else if (jsonBody["id"] == led_resource.id)
            //   http_rest_server.send(409);
        }
        else if (server.method() == HTTP_PUT) {
          Serial.println("PUT!!!!!!");
            // if (jsonBody["id"] == led_resource.id) {
            //     json_to_resource(jsonBody);
            //     http_rest_server.sendHeader("Location", "/leds/" + String(led_resource.id));
            //     http_rest_server.send(200);
            //     digitalWrite(led_resource.gpio, led_resource.status);
            // }
            // else
            //   http_rest_server.send(404);
        }
    }
}

void config_rest_server_routing() {
    server.on("/", HTTP_GET, []() {
        server.send(200, "text/html",
            "Welcome to the ESP8266 REST Web Server");
    });
    server.on("/leds", HTTP_GET, get_leds);
    server.on("/leds", HTTP_POST, post_put_leds);
    server.on("/leds", HTTP_PUT, post_put_leds);
}


//////////WIFI//////////////



//          ______
//         | back |
//         |  TX  |
//  ______ |______| ______
// | left ||  top || right|
// |  D3  ||  RX  ||  D5  |
// |______||______||______|
//         | front|
//         |  D6  |
//         |______|
//         |bottom|
//         |  D7  |
//         |______|

#define _top RX
#define _bottom D7
#define _left D3
#define _right D5
#define _front D6
#define _back TX



Adafruit_NeoMatrix TOP = Adafruit_NeoMatrix(8, 8, _top,
NEO_MATRIX_BOTTOM + NEO_MATRIX_LEFT +
NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix BOTTOM = Adafruit_NeoMatrix(8, 8, _bottom,
NEO_MATRIX_TOP + NEO_MATRIX_RIGHT +
NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix LEFT = Adafruit_NeoMatrix(8, 8, _left,
NEO_MATRIX_BOTTOM + NEO_MATRIX_RIGHT +
NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix RIGHT = Adafruit_NeoMatrix(8, 8, _right,
NEO_MATRIX_BOTTOM + NEO_MATRIX_RIGHT +
NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix FRONT = Adafruit_NeoMatrix(8, 8, _front,
NEO_MATRIX_BOTTOM + NEO_MATRIX_RIGHT +
NEO_MATRIX_ROWS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix BACK = Adafruit_NeoMatrix(8, 8, _back,
NEO_MATRIX_TOP + NEO_MATRIX_RIGHT +
NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE,
NEO_GRB            + NEO_KHZ800);

Adafruit_NeoMatrix matrix[] = {TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK};

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);
    uint16_t myRemapFn(uint16_t x, uint16_t y) {
      return 8 * y + x;
    }

    


void batteryInfo(Adafruit_NeoMatrix matrix, int baterryValue){

matrix.clear();

int baterry[8][8] = {  
   {0, 0, 0, 0, 0, 0, 0, 0},
   {0, 0, 0, 0, 0, 0, 0, 0},
   {1, 1, 1, 1, 1, 1, 1, 0},
   {1, 2, 2, 3, 3, 4, 1, 1},
   {1, 2, 2, 3, 3, 4, 1, 1},
   {1, 1, 1, 1, 1, 1, 1, 0},
   {0, 0, 0, 0, 0, 0, 0, 0},
   {0, 0, 0, 0, 0, 0, 0, 0}
  };

for(int row = 0; row < 8; row++) {
    for(int column = 0; column < 8; column++) {
     if(baterry[row][column] == 1) {
       matrix.drawPixel(column, row, matrix.Color(white.r, white.g, white.b)); 
       matrix.show();
       //fadePixel(column, row, red, white, 240, 1);
     }
   }
}

switch (baterryValue) {
   case 5: 
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
                if(baterry[row][column] == 2) {
                matrix.drawPixel(column, row, matrix.Color(red.r, red.g, red.b)); 
                matrix.show();
                }
            }
        }
     break;
   case 6: 
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
                if(baterry[row][column] == 2) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
                if(baterry[row][column] == 3) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
            }
        }
     break;   
   case 7: 
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
                if(baterry[row][column] == 2) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
                if(baterry[row][column] == 3) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
                if(baterry[row][column] == 4) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
            }
        }
     break;

     case 8: 
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
              for(int row = 0; row < 8; row++) {
                          for(int column = 0; column < 8; column++) {
                              if(baterry[row][column] == 1) {
                              matrix.drawPixel(column, row, matrix.Color(blue.r, blue.g, blue.b)); 
                              matrix.show();
                              //fadePixel(column, row, red, white, 240, 1);
                              }
                          }
                      }
                if(baterry[row][column] == 2) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
                if(baterry[row][column] == 3) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
                if(baterry[row][column] == 4) {
                matrix.drawPixel(column, row, matrix.Color(green.r, green.g, green.b)); 
                matrix.show();
                }
            }
        }
     break;
   
   default: 
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
                if(baterry[row][column] == 1) {
                matrix.drawPixel(column, row, matrix.Color(white.r, white.g, white.b)); 
                matrix.show();
                //fadePixel(column, row, red, white, 240, 1);
                }
            }
        }
     break;
 }
}


int readVoltage(){
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 7.4V):
  int voltage = int(sensorValue * (9.3 / 1023.0));
  // print out the value you read:
  //Serial.println(voltage);

  return voltage;
}

int baterryValue_old = readVoltage();

void setup(void) 
{
Serial.begin(115200);


//wifi


WiFi.mode(WIFI_STA);
WiFi.begin(ssid, password);
// Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });
  server.onNotFound(handleNotFound);
  server.begin();
  Serial.println("HTTP server started");

//timer
batteryTimer.begin(SECS(5));
batteryInfo(FRONT, baterryValue_old);


//********** CHANGE PIN FUNCTION  TO GPIO **********
//GPIO 1 (TX) swap the pin to a GPIO.
//pinMode(TX, FUNCTION_3); 

//GPIO 3 (RX) swap the pin to a GPIO.
//pinMode(RX, FUNCTION_3); 
//**************************************************

initCube(matrix);
clearCube(matrix);  


  
  Serial.println("Accelerometer Test"); Serial.println("");

  // if(!accel.begin())
  // {
  //   /* There was a problem detecting the ADXL345 ... check your connections */
  //   //Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
  //   //while(1);
  // }
  //accel.setRange(ADXL345_RANGE_16_G); 
}

struct angles {
   int x;
   int y;
   int z;
};

struct angles getADXLval() {
   struct angles ang;

  sensors_event_t event; 
  accel.getEvent(&event);

   ang.x = (int)event.acceleration.x;
   ang.y = (int)event.acceleration.y;
   ang.z = (int)event.acceleration.z;

   return ang;
}


void loop(void) 
{


server.handleClient();
MDNS.update();

  //struct angles ADXLval;
  //ADXLval = getADXLval();

  


// if (batteryTimer.available())
//   {
    
//     int baterryValue_new = readVoltage();
// if (baterryValue_new != baterryValue_old) {
//   baterryValue_old = baterryValue_new;
//   batteryInfo(FRONT, baterryValue_old);
//   }

//     batteryTimer.restart();
//   }

}



