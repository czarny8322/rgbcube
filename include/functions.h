
void initCube(Adafruit_NeoMatrix cube[]){
  for (int i=0; i<6; i++) {   
    cube[i].begin();
    cube[i].setBrightness(30);
    cube[i].setTextColor( cube[i].Color(255, 255, 255) );
    cube[i].setTextWrap(false);   
   }
}

void clearCube(Adafruit_NeoMatrix cube[]){
  for (int i=0; i<6; i++) {   
     
    cube[i].clear();
   }
}



void test_cubeOrientation(Adafruit_NeoMatrix TOP, Adafruit_NeoMatrix BOTTOM, Adafruit_NeoMatrix LEFT, Adafruit_NeoMatrix RIGHT, Adafruit_NeoMatrix FRONT, Adafruit_NeoMatrix BACK){
   
   //TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK
    TOP.drawPixel(0, 0, TOP.Color(red.r, red.g, red.b));
    TOP.drawChar(2,0,'T',128,0,1);
    TOP.show();
    TOP.clear();

    BOTTOM.drawPixel(0, 0, BOTTOM.Color(red.r, red.g, red.b));
    BOTTOM.drawChar(2,0,'B',128,0,1);
    BOTTOM.show();
    BOTTOM.clear();

    LEFT.drawPixel(0, 0, LEFT.Color(red.r, red.g, red.b));
    LEFT.drawChar(2,0,'L',128,0,1);
    LEFT.show();
    LEFT.clear();

    RIGHT.drawPixel(0, 0, RIGHT.Color(red.r, red.g, red.b));
    RIGHT.drawChar(2,0,'R',128,0,1);
    RIGHT.show();
    RIGHT.clear();

    FRONT.drawPixel(0, 0, FRONT.Color(red.r, red.g, red.b));
    FRONT.drawChar(2,0,'F',128,0,1);
    FRONT.show();
    FRONT.clear();

    BACK.drawPixel(0, 0, BACK.Color(red.r, red.g, red.b));
    BACK.drawChar(2,0,'B',128,0,1);
    BACK.show();
    BACK.clear();
}




void getCornersXY(Adafruit_NeoMatrix matrix[]){

  for (int i=0 ; i <6 ; i++)
  {
     matrix[i].drawPixel(0, 0, matrix[i].Color(red.r, red.g, red.b)); 
    matrix[i].show();
    matrix[i].clear(); 
  }
}










